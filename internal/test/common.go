package test

import (
	"fmt"

	"gitlab.com/wmf508/cia/repository"
	"gitlab.com/wmf508/cia/server"
)

func GetConfig(configPath string) (server.Config, error) {
	config, err := server.LoadConfig(configPath)
	if err != nil {
		return server.Config{}, fmt.Errorf("failed to load config in test: %w", err)
	}
	return config, nil
}

func GetRepository(config server.Config) (*repository.Repository, error) {
	repo, err := server.GetRepository(config)
	if err != nil {
		return nil, err
	}
	return repo, nil
}
