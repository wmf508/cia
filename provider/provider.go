package provider

import "gitlab.com/wmf508/cia/config"

// provider type enum
const (
	// node provider key in the configuration file
	ProviderKey = "provider"
	// node provider type key in the configuration file
	NodeProviderType = config.NodeType
	// pod provider type key in the configuration file
	PodProviderType = config.PodType
	// service provider type key in the configuration file
	ServiceProviderType = config.ServiceType
	// namespace provider type key in the configuration file
	NamespaceProviderType = config.ServiceType
	// prometheus
	PrometheusProvider = "prometheus"
)
