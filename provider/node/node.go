package node

import (
	commonK8sNode "gitlab.com/wmf508/alfredy/kubernetes/node"
)

type NodeProvider interface {
	GetNodes() ([]*commonK8sNode.Node, error)
}
