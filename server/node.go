package server

import (
	"errors"
	"fmt"
	"sync"
	"time"

	"gitlab.com/wmf508/cia/provider"
	nodeProv "gitlab.com/wmf508/cia/provider/node"
	nodeSvc "gitlab.com/wmf508/cia/service/node"
	promPubProv "gitlab.com/wmf508/cia_provider_prometheus/provider"
	prometheusNodeProv "gitlab.com/wmf508/cia_provider_prometheus/provider/node"
)

type NodeSubServer struct {
	Service *nodeSvc.NodeService
}

// get node provider and create node sub-server
func GetNodeServer(nodeConfig ProviderItemConfig) (*NodeSubServer, error) {

	prov, err := getNodeProvider(nodeConfig)
	if err != nil {
		return nil, fmt.Errorf("failed to create node provider: %w", err)
	}

	svc := &nodeSvc.NodeService{
		Prov: prov,
	}

	svr := &NodeSubServer{
		Service: svc,
	}

	return svr, nil

}

func getNodeProvider(nodeConfig ProviderItemConfig) (nodeProv.NodeProvider, error) {
	var prov nodeProv.NodeProvider
	var err error

	switch nodeConfig.Type {
	case provider.PrometheusProvider:
		config := promPubProv.ProviderConfig{
			Address:  nodeConfig.Address,
			Port:     nodeConfig.Port,
			Protocol: nodeConfig.Protocol,
		}
		prov, err = prometheusNodeProv.NewNodeProvider(config)
		if err != nil {
			// TODO: add error code
			log.Printf("failed to create prometheus provider: %v", err)
			return nil, err
		}
	default:
		return nil, errors.New("invalid node privider type")
	}
	return prov, nil
}

func (svr *NodeSubServer) Run(wg *sync.WaitGroup, canncel chan bool) {
	defer wg.Done()

	log.Println("node sub server running")

	// update all nodes in beginning
	svr.Service.UpdateAllNodes()

	// start collect all node at interval
	go func() {
		defer wg.Done()
		ticker := time.NewTicker(30 * time.Minute)
		for range ticker.C {
			err := svr.Service.UpdateAllNodes()
			if err != nil {
				// TODO: add error code
				log.Printf("failed to update all nodes: %v", err.Error())
			}
		}
	}()
	wg.Add(1)

	// start collect node at interval
	go func() {
		defer wg.Done()
		ticker := time.NewTicker(5 * time.Second)
		for range ticker.C {
			err := svr.Service.UpdateNodes()
			if err != nil {
				// TODO: add error code
				log.Printf("failed to update nodes: %v", err.Error())
			}
		}
	}()
	wg.Add(1)
}
