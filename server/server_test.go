package server

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNewMainServer(t *testing.T) {
	mainSvr, err := NewMainServer("../config")
	require.NoError(t, err)
	require.NotEmpty(t, mainSvr)
}
