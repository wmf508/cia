package server

import "sync"

type SubServer interface {
	Run(wg *sync.WaitGroup, cannel chan bool)
}

type MainServer struct {
	SubServers map[string]SubServer
}

// structure about yaml config
type Config struct {
	Providers  ProvidersConfig  `mapstructure:"providers"`
	Repository RepositoryConfig `mapstructure:"repository"`
}

type ProvidersConfig struct {
	Node ProviderItemConfig `mapstructure:"node"`
	Pod  ProviderItemConfig `mapstructure:"pod"`
}

type RepositoryConfig struct {
	Type     string `mapstructure:"type"`
	Address  string `mapstructure:"address"`
	Port     int    `mapstructure:"port"`
	Username string `mapstructure:"username"`
	Password string `mapstructure:"password"`
}

type ProviderItemConfig struct {
	Type     string `mapstructure:"type"`
	Address  string `mapstructure:"address"`
	Port     int    `mapstructure:"port"`
	Protocol string `mapstructure:"protocol"`
}
