package server

import (
	"errors"
	"flag"
	"fmt"
	"strings"
	"sync"

	"github.com/spf13/viper"
	storageMongo "gitlab.com/wmf508/alfred/database/mongodb"
	commonlog "gitlab.com/wmf508/alfredy/log"
	commonConfig "gitlab.com/wmf508/cia/config"
	"gitlab.com/wmf508/cia/repository"
	mongodRepo "gitlab.com/wmf508/cia_repository_mongodb/repository"
)

var log = commonlog.Logger

const (
	// 当启用-dev命令行参数时使用的开发环境配置文件路径, 注意是相对于makefile文件路径
	devConfigPath = "./config"
	// 生产环境中使用的配置文件路径
	prodConfigPath = ""
)

// load configuration from file and enviroment
func LoadConfig(configPath string) (Config, error) {
	var config = Config{}

	viper.AddConfigPath(configPath)
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")

	viper.AutomaticEnv()
	err := viper.ReadInConfig()
	if err != nil {
		return Config{}, fmt.Errorf("failed to read config file by viper: %w", err)
	}

	err = viper.Unmarshal(&config)
	if err != nil {
		return Config{}, fmt.Errorf("filed to unmarshal config by viper: %w", err)
	}

	return config, err
}

func NewMainServer(configPath ...string) (*MainServer, error) {
	dev := false
	flag.BoolVar(&dev, "dev", false, "development mode")
	flag.Parse()

	path := prodConfigPath

	// 开发环境下使用开发环境配置文件路径
	if dev {
		path = devConfigPath
	}

	// 一般用于单元测试
	if configPath != nil {
		path = configPath[0]
	}

	config, err := LoadConfig(path)
	if err != nil {
		return nil, fmt.Errorf("failed to load config: %w", err)
	}

	// get sub-servers
	subSvrs, err := getSubServers(config)
	if err != nil {
		// TODO: add error code
		log.Printf("failed to create sub-servers: %v", err)
	}

	svr := &MainServer{
		SubServers: subSvrs,
	}

	if len(subSvrs) > 0 {
		return svr, nil
	}

	return nil, errors.New("all sub-server create failed")
}

// get all sub-servers
func getSubServers(config Config) (map[string]SubServer, error) {
	subSvrs := make(map[string]SubServer)

	if config.Providers == (ProvidersConfig{}) {
		// 没有配置任何provider
		return nil, errors.New("no any provider in configuration")
	}

	repo, err := GetRepository(config)
	if err != nil {
		return nil, fmt.Errorf("failed to create repository: %w", err)
	}

	// get all sub-servers and add repository instance to it
	err = addRepoToService(config, subSvrs, repo)

	return subSvrs, err
}

func GetRepository(config Config) (*repository.Repository, error) {
	// create node repository
	if config.Repository == (RepositoryConfig{}) {
		// 如果config.Providers.Node是空结构体, 说明没有配置node provider
		return nil, errors.New("no repository in configuration file")
	}

	var err error

	switch config.Repository.Type {
	// mongodb
	case repository.MongodbRepository:
		var repo *repository.Repository
		repo, err = GetMongoRepository(config)
		if err == nil {
			return repo, nil
		}
	default:
		return nil, errors.New("invalid repository type")
	}

	return nil, err
}

// connect to mongodb
func GetMongoRepository(config Config) (*repository.Repository, error) {
	repoConfig := storageMongo.Config{
		Address:  config.Repository.Address,
		Port:     config.Repository.Port,
		Username: config.Repository.Username,
		Password: config.Repository.Password,
	}
	mainRepos, err := mongodRepo.NewRepository(repoConfig)
	if err != nil {
		return nil, fmt.Errorf("failed to create mongodb repository: %w", err)
	}
	log.Println("connect mongodb success")
	return mainRepos, nil
}

func addRepoToService(config Config, subSvrs map[string]SubServer, repo *repository.Repository) error {
	// TODO: 将几个错误合成一个
	errs := []string{}

	// create node server
	if config.Providers.Node == (ProviderItemConfig{}) {
		// 如果config.Providers.Node是空结构体, 说明没有配置node provider
		return errors.New("there is no node provider on the configuration file")
	} else {
		svr, err := GetNodeServer(config.Providers.Node)
		if err != nil {
			errs = append(errs, fmt.Errorf("failed to create node sub-server: %v", err).Error())
		} else {
			svr.Service.Repo = repo.Node
			subSvrs[commonConfig.NodeType] = svr
		}
	}

	if len(errs) > 0 {
		// 从第一个错误开始换行显示
		errs[0] = "\n" + errs[0]
		return errors.New(strings.Join(errs, "\n"))
	}

	return nil
}

// start main server
func (svr *MainServer) Run() {
	var wg sync.WaitGroup
	cancelChanMap := make(map[string]chan bool)

	// 为每个sub-server创建chan用于关闭对应go routine
	cancelChanMap[commonConfig.NodeType] = make(chan bool)

	// start sub-servers
	for svrType, subSvr := range svr.SubServers {

		switch svrType {
		case "node":
			go subSvr.Run(&wg, cancelChanMap[commonConfig.NodeType])
		}

		wg.Add(1)
	}

	// TODO: 通过控制channel关闭对应sub-server的逻辑(可选)

	log.Println("main server running")
	wg.Wait()
}
