package config

// type enum
const (
	NodeType      = "node"
	PodType       = "pod"
	ServiceType   = "service"
	NamespaceType = "namespace"
)
