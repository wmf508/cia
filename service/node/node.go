package node

import (
	"fmt"

	commonlog "gitlab.com/wmf508/alfredy/log"
)

var log = commonlog.Logger

func NewNodeService() *NodeService {

	return &NodeService{}
}

func (svc *NodeService) UpdateAllNodes() error {
	nodes, err := svc.Prov.GetNodes()
	if err != nil {
		return fmt.Errorf("failed to get nodes by provider: %w", err)
	}

	_, err = svc.Repo.UpdateNodesCreateNoExist(nodes)

	if err != nil {
		return fmt.Errorf("failed to update nodes: %w", err)
	}

	log.Println("all nodes infomation updated")

	return nil
}

func (svc *NodeService) UpdateNodes() error {
	// TODO: implement update nodes
	log.Println("update nodes")
	return nil
}
