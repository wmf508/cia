package node_test

import (
	"log"
	"testing"

	"gitlab.com/wmf508/cia/internal/test"
	"gitlab.com/wmf508/cia/repository"
	"gitlab.com/wmf508/cia/server"
)

var (
	config server.Config
	repo   *repository.Repository
	srv    *server.NodeSubServer
)

func TestMain(m *testing.M) {
	var err error

	config, err = test.GetConfig("../../config")
	if err != nil {
		log.Println("failed to get config")
	}

	repo, err = test.GetRepository(config)
	if err != nil {
		log.Println("failed to get repository")
	}

	srv, err = server.GetNodeServer(config.Providers.Node)
	if err != nil {
		log.Println("failed to get server")
	}

	m.Run()
}

func TestUpdateAllNodes(t *testing.T) {

	srv.Service.Repo = repo.Node

	srv.Service.UpdateAllNodes()
}
