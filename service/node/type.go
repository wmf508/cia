package node

import (
	nodeProv "gitlab.com/wmf508/cia/provider/node"
	nodeRepo "gitlab.com/wmf508/cia/repository/node"
)

type NodeService struct {
	Prov nodeProv.NodeProvider
	Repo nodeRepo.NodeRepository
}
