package main

import (
	"log"
	"os"

	"gitlab.com/wmf508/cia/server"
)

func main() {
	log.SetOutput(os.Stdout)

	cli, err := server.NewMainServer()
	if err != nil {
		log.Fatal("failed to create main server")
	}

	log.Println("start the server...")
	cli.Run()
	log.Println("server shut down!")
}
