package node

import (
	commonNode "gitlab.com/wmf508/alfredy/kubernetes/node"
)

type NodeRepository interface {
	UpdateNodesCreateNoExist([]*commonNode.Node) (int64, error)
}
