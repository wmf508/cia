package repository

import nodeRepo "gitlab.com/wmf508/cia/repository/node"

type Repository struct {
	Node nodeRepo.NodeRepository
}

const (
	MongodbRepository = "mongodb"
)
