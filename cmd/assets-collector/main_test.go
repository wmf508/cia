package main

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/wmf508/alfredy/errorcode"
)

func TestErrorCode(t *testing.T) {
	desc, err := errorcode.GetDesc("E001010001")
	require.NoError(t, err)
	require.NotEmpty(t, desc)

	// pass incorrect errorcode
	desc, err = errorcode.GetDesc("A001010001")
	require.NotEmpty(t, err)
	require.Empty(t, desc)
}
