package main

import (
	"os"

	"gitlab.com/wmf508/alfredy/errorcode"
	commonlog "gitlab.com/wmf508/alfredy/log"
	"gitlab.com/wmf508/cia/server"
)

var log = commonlog.Logger

func main() {

	cli, err := server.NewMainServer()
	if err != nil {
		desc, e := errorcode.GetDesc("E001010001")
		if e != nil {
			log.Println(e.Error())
		}
		log.Printf("%s: %v", desc.GetLogMsg(), err.Error())
		os.Exit(1)
	}

	log.Println("start the server...")
	cli.Run()
	log.Println("server shut down!")
}
