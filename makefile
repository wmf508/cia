dev:
	go run ./cmd/assets-collector/main.go -dev
test:
	@go test -cover ./...
update:
	go get -u gitlab.com/wmf508/cia_provider_prometheus/provider
	go get -u gitlab.com/wmf508/cia_repository_mongodb/repository
	go get -u gitlab.com/wmf508/alfred/database/mongodb
	go get -u gitlab.com/wmf508/alfredy/kubernetes/node
	go get -u gitlab.com/wmf508/ghost-rider
errorcode:
	go get -u gitlab.com/wmf508/alfredy
.PHONY: test dev update